﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

connection.on("ReceiveMessage", function (user, message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = "<b>" + user + "</b>"+ " says " + msg;
    var li = document.createElement("li");
    li.innerHTML = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
});

connection.on("SetCurrentDateTime", function (currentDateTime) {
    var currentTime = document.getElementById("currentTime");
    var startDate2 = moment(currentDateTime, 'YYYY-MM-DD HH:mm:ss').toDate();
    currentTime.innerText = startDate2;
});

connection.start().then(() => {
    //setInterval(function () {
    //    connection.invoke("GetCurrentDateTime").catch(function (err) {
    //        return console.error(err.toString());
    //    });
    //}, 1000);
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

