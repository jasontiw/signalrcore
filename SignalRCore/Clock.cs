﻿using Microsoft.AspNetCore.SignalR;
using SignalRCore.Hub;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Timers;

namespace SignalRCore
{
	public class Clock
	{
		private static IHubContext<ChatHub> _hubContext;
		private static object _mutex = new object();
		private static Clock _instance;
		private Timer timer;
		private Clock(IHubContext<ChatHub> hubContext)
		{
			_hubContext = hubContext;
			timer = new Timer(1000);
			timer.Elapsed += Timer_Elapsed;
			timer.Start();
		}

		private void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			_hubContext.Clients.All.SendAsync("SetCurrentDateTime", DateTime.Now.ToString("O"));
			//GlobalHost.ConnectionManager.GetHubContext<ClockHub>().Clients.All.sendTime(DateTime.UtcNow.ToString());
		}

		public static Clock Instance(IHubContext<ChatHub> hubContext)
		{
			if (_instance == null)
			{
				lock (_mutex) // now I can claim some form of thread safety...
				{
					_instance = new Clock(hubContext);
				}
			}
			return _instance;
		}
	}
}
