﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace SignalRCore.Hub
{
	public class ChatHub : Microsoft.AspNetCore.SignalR.Hub
	{
		public async Task SendMessage(string user, string message)
		{
			await Clients.All.SendAsync("ReceiveMessage", user, message);
		}
		public async Task GetCurrentDateTime()
		{
			await Clients.Caller.SendAsync("SetCurrentDateTime", DateTime.Now.ToString("O"));
		}

	}
}
